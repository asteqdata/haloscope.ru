<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Socialite;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $user = Auth::user();

        echo '<pre>';
        print_r($user->remember_token);
        echo '</pre>';


        //$user = Socialite::driver('vkontakte')->user();
        //echo $user->accessTokenResponseBody;

        return view('home');
    }
}
